<br>
<center>
  <h2>servicios</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/servicios/nuevo">Agregar Nuevo</a>
</center>

<?php if ($listadoServicios): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">IDENTIFICACION</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoServicios->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_serv; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE IDENTIFICACION-->
          <td class="text-center">
            <?php echo $filaTemporal->identificacion_serv; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE NOMBRE-->
          <td class="text-center">
            <?php echo $filaTemporal->nombre_serv; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE TELEFONO-->
          <td class="text-center">
            <?php echo $filaTemporal->telefono_serv; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE DIRECCION-->
          <td class="text-center">
            <?php echo $filaTemporal->direccion_serv; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE EMAIL-->
        <td class="text-center">
          <?php echo $filaTemporal->email_serv; ?>
        </td>
        <!--LLAMADO A LOS DATOS DE ESTADO-->
      <td class="text-center">
        <?php echo $filaTemporal->estado_serv; ?>
      </td>
      <td class="text-center">
        <?php echo $filaTemporal->email_reg; ?>
      </td>
    </td>
    <!--OPCIONES EDITAR Y ELIMINAR-->
    <td class="text-center">

      <a class="btn btn-success"  href="<?php echo site_url(); ?>/servicio/editar/<?php echo $filaTemporal->id_serv; ?>">
        <i class="fa fa-pen"></i>
      </a>
      <a  onclick="return confirm('ESTAS SEGURO DE ELIMINAR ');"   href="<?php echo site_url(); ?>/servicio/procesarEliminacion/<?php echo $filaTemporal->id_serv; ?>" class ="btn btn-danger">
        <i class="fa fa-trash"></i>
      </a>
    </td>
  </tr>

<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<div class="alert alert-danger">
<h1>NO SE ENCONTRARON SERVICIOS</h1>
</div>
<?php endif; ?>


<script type="text/javascript">
function confirmarEliminacion(id_serv){
iziToast.question({
timeout: 10000,
close: false,
overlay: true,
displayMode: 'once',
id: 'question',
zindex: 999,
title: 'CONFIRMACION',
message: 'ESTAS SEGURO DE ELIMINAR',
position: 'center',
buttons: [
  ['<button><b>SI BB</b></button>', function (instance, toast) {

      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
      window.location.href="<?php echo site_url(); ?>/servicio/procesarEliminacion/"+ id_serv;

  }, true],
  ['<button>NO BB</button>', function (instance, toast) {

      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

  }],
]
});

}

</script>

<script type="text/javascript">
$("#tbl-servicio").DataTable();
</script>
