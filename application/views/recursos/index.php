<div class="col-md-12">
<br>
<center>
  <h2> RECURSOS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/recursos/nuevo">Agregar Nuevo Recurso</a>
</center>

<?php if ($listadoRecurso): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table " id="tbl-recursos">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">CONTACTO</th>
        <th class="text-center">CATALOGOS</th>
        <th class="text-center">FACTURACION</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoRecursos->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_rec; ?>
          </td>
          <!--LLAMADO A LOS DATOS CONTACTOS-->
          <td class="text-center">
            <?php echo $filaTemporal->contacto_rec; ?>
          </td>
            <!--LLAMADO A LOS DATOS CTALOGO-->
          <td class="text-center">
            <?php echo $filaTemporal->catalogo_rec; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE FACTURACION-->
          <td class="text-center">
            <?php echo $filaTemporal->facturacion_rec; ?>
          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
            <a href="<?php echo site_url() ?>/clientes/editar/<?php echo $filaTemporal->id_cli ?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
            <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cli; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON NINGUN RECURSO</h1>
  </div>
<?php endif; ?>
</div>

<script type="text/javascript">
    function confirmarEliminacion(id_cli){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el recurso de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/clientes/procesarEliminacion/"+id_cli;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
// incorporar botones de Exportacion
$("#tbl-recurso").DataTable();
</script>
