<br>
<h1>
  <center>Edicion de Registro de clientes</center>
</h1>

<form action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post">
  <!--CAMBIAR ESTO PARA QUE APARESCA EL ID Y CAMBIAR ARRIBITA NOMAS -->
    <input type="hidden" name="id_cli" id="id_cli" value="<?php echo $cliente->id_cli; ?>">
    <br>
    <b>Identificación: </b>
    <br>
    <input type="number" class="form-control" name="identificacion_cli" id="identificacion_cli" value="<?php echo $cliente->identificacion_cli; ?>" placeholder="Por favor ingrese la identificacion" class="form-control input-sm " required>
    <br>
    <b>Nombre: </b>
    <br>
    <input type="text" class="form-control" name="nombre_cli" id= "nombre_cli" value="<?php echo $cliente->nombre_cli; ?>" placeholder="Ingrese su nombre" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Apellido: </b>
    <br>
    <input type="text" class="form-control" name="apellido_cli" id= "apellido_cli" value="<?php echo $cliente->apellido_cli; ?>" placeholder="Ingrese el apellido" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Direccion: </b>
    <br>
    <input type="text" class="form-control" name="direccion_cli" id= "direccion_cli" value="<?php echo $cliente->direccion_cli; ?>" placeholder="Ingrese su direccion" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Telefono: </b>
    <br>
    <input type="text" class="form-control"  name="telefono_cli" id= "telefono_cli" value="<?php echo $cliente->telefono_cli; ?>" placeholder="Ingrese su numero de telefono" class="form-control input-sm " required >
    <br>
    <label class="control-label" for="">ESTADO</label>

    <select  class="form-control" name="estado_cli" id="estado_cli">
        <option value="">SELECIONE</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button"> <i class="fa-solid fa-floppy-disk"></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/clientes/index"> <strong style="color:white;"><i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>
    <script type="text/javascript">
  //activando el estado seleccionado para el cliente
    $('#estado_cli').val('<?php echo $cliente->estado_cli; ?>');
  </script>
</form>
