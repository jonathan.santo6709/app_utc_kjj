<br>
<h1> <center>Registro de clientes</center> </h1>
<form  action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="frm_nuevo_cliente">

    <b>Identificación: </b>
    <br>
    <input type="number" class="form-control" name="identificacion_cli" id="identificacion_cli" value="" placeholder="Por favor ingrese la identificacion" class="form-control input-sm " required>
    <br>
    <b>Nombre: </b>
    <br>
    <input type="text" class="form-control" name="nombre_cli" id= "nombre_cli"value="" placeholder="Ingrese su nombre" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Apellido: </b>
    <br>
    <input type="text" class="form-control" name="apellido_cli" id= "apellido_cli"value="" placeholder="Ingrese el apellido" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Direccion: </b>
    <br>
    <input type="text" class="form-control" name="direccion_cli" id= "direccion_cli"value="" placeholder="Ingrese su direccion" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Telefono: </b>
    <br>
    <input type="text" class="form-control"  name="telefono_cli" id= "telefono_cli"value="" placeholder="Ingrese su numero de telefono" class="form-control input-sm " required >
    <br>
    <label class="control-label" for="">ESTADO</label>

    <select  class="form-control" name="estado_cli" id="estado_cli">
        <option value="">SELECIONE</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <button type="submit" name="button"  class="btn btn-primary">GUARDAR</button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
    <button type="button" name="button" a href="<?php echo site_url(); ?>/clientes/index" class= "btn btn-secondary">CANCELAR</button>

</form>
<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        identificacion_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        apellido_cli:{
          required:true,
          letras:true
        },
        nombre_cli:{
          required:true,
          letras:true
        },
        telefono_cli:{
          required:true,
          minlength:8,
          maxlength:10,
          digits:true
        },
        direccion_cli:{
          required:true
        }
      },
      messages:{
        identificacion_cli:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        apellido_cli:{
          required:"Ingrese su apellido"
        },
        nombre_cli:{
          required:"Ingrese su nombre"
        },
        telefono_cli:{
          required:"Por favor ingrese su numero telefonico",
          minlength:"Su numero telefonico debe tener minimo 8 numeros si es fijo",
          maxlength:"Su numero telefonico debe tener maximo 10 numeros si es movil",
          digits:"La cédula solo acepta números"
        },
        direccion_cli:{
          required:"Ingrese su direccion de domicilio"
        }
      }
    });
</script>
