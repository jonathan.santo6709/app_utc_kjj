<br>
<center>
  <h2>LISTADO DE PRODUCTOS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/productos/nuevo">
    <i class="fa fa-plus-circle"></i>
    Agregar Nuevo Producto
  </a>
</center>

<?php if ($listadoProductos): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">CATEGORIA</th>
        <th class="text-center">MARCA</th>
        <th class="text-center">STOCK</th>
        <th class="text-center">ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoProductos->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_pro; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE NOMBRE-->
          <td class="text-center">
            <?php echo $filaTemporal->nombre_pro; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE  CATEGORIA-->
          <td class="text-center">
            <?php echo $filaTemporal->categoria_pro; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE MARCA-->
          <td class="text-center">
            <?php echo $filaTemporal->marca_pro; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE STOCK-->
          <td class="text-center">
            <?php echo $filaTemporal->stock_pro; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE ESTADO-->
          <td class="text-center">
            <?php if ($filaTemporal->estado_pro=="DISPONIBLE"): ?>
              <div class="alert alert-success">
                <?php echo $filaTemporal->estado_pro; ?>
              </div>
            <?php else: ?>
              <div class="alert alert-danger">
                <?php echo $filaTemporal->estado_pro; ?>
              </div>
            <?php endif; ?>
          </td>
          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
            <a class="btn btn-success"  href="<?php echo site_url(); ?>/productos/editar/<?php echo $filaTemporal->id_pro; ?>">
              <i class="fa fa-pen"></i>
            </a>
            <a href="javascript:void(0)"
            onclick="confirmarEliminacion('<?php echo $filaTemporal->id_pro; ?>');"
            class="btn btn-danger">
            <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON PRODUCTOS REGISTRADOS</h1>
  </div>
<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_pro){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/productos/procesarEliminacion/"+ id_pro;

        }, true],
        ['<button>NO </button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>

<script type="text/javascript">
  $("#tbl-clientes").DataTable();
</script>
