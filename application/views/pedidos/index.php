<br>
<center>
  <h2>LISTADO DE PEDIDOS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/pedidos/nuevo">
    <i class="fa fa-plus-circle"></i>
    Agregar Nuevo Pedidos
  </a>
</center>

<?php if ($listadoPedidos): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">MONTO</th>
        <th class="text-center">DESCRIPCION</th>
        <th class="text-center">ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoPedidos->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_ped; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE FECHA-->
          <td class="text-center">
            <?php echo $filaTemporal->fecha_ped; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE MONTO-->
          <td class="text-center">
            <?php echo $filaTemporal->monto_ped; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE DESCRIPCION-->
          <td class="text-center">
            <?php echo $filaTemporal->descripcion_ped; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE ESTADO-->
          <td class="text-center">
            <?php if ($filaTemporal->estado_ped=="ENTREGADO"): ?>
              <div class="alert alert-success">
                <?php echo $filaTemporal->estado_ped; ?>
              </div>
            <?php else: ?>
              <div class="alert alert-danger">
                <?php echo $filaTemporal->estado_ped; ?>
              </div>
            <?php endif; ?>
          </td>
          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
            <a class="btn btn-success"  href="<?php echo site_url(); ?>/pedidos/editar/<?php echo $filaTemporal->id_ped; ?>">
              <i class="fa fa-pen"></i>
            </a>
            <a href="javascript:void(0)"
            onclick="confirmarEliminacion('<?php echo $filaTemporal->id_ped; ?>');"
            class="btn btn-danger">
            <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON PEDIDOS REGISTRADOS</h1>
  </div>
<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_ped){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/pedidos/procesarEliminacion/"+ id_ped;

        }, true],
        ['<button>NO </button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>

<script type="text/javascript">
  $("#tbl-clientes").DataTable();
</script>
