<br>
<h1>
  <center>Registro de pedidos</center>
</h1>
<form action="<?php echo site_url(); ?>/pedidos/procesarActualizacion" method="post">
  <!--CAMBIAR ESTO PARA QUE APARESCA EL ID Y CAMBIAR ARRIBITA NOMAS -->
    <input type="hidden" name="id_ped" id="id_ped" value="<?php echo $pedido->id_ped; ?>">
    <b>Fecha: </b>
    <br>
    <input type="date" class="form-control" name="fecha_ped" id="fecha_ped" value="<?php echo $pedido->fecha_ped; ?>" placeholder="Por favor ingrese la fecha" class="form-control input-sm " required>
    <br>
    <b>Monto: </b>
    <br>
    <input type="number" class="form-control" name="monto_ped" id= "monto_ped" value="<?php echo $pedido->monto_ped; ?>" placeholder="Ingrese la cantidad" class="form-control input-sm" >
    <br>
    <b>Descripcion: </b>
    <br>
    <input type="text" class="form-control" name="descripcion_ped" id= "descripcion_ped"value="<?php echo $pedido->descripcion_ped; ?>" placeholder="Ingrese alguna descripcion" class="form-control input-sm " required requerid pattern="[A-Za-z-ñ]+">
    <br>
    <label class="control-label" for="">ESTADO</label>

    <select  class="form-control" name="estado_ped" id="estado_ped">
        <option value="">SELECIONE</option>
        <option value="ENTREGADO">ENTREGADO</option>
        <option value="PENDIENTE">PENDIENTE</option>
    </select>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button"> <i class="fa-solid fa-floppy-disk"></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/pedidos/index"> <strong style="color:white;"><i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>
    <script type="text/javascript">
  //activando el estado seleccionado para el cliente
    $('#estado_ped').val('<?php echo $pedido->estado_ped; ?>');
  </script>
</form>
