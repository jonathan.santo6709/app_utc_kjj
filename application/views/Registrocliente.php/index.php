<br>
<center>
  <h2>Registroclientes</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/registrocliente/nuevo">
    <i class="fa fa-plus-circle"></i>
    Agregar Nuevo Cliente
  </a>
</center>

<?php if ($listadoRegistrocliente): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover" id="tbl-registrocliente">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">IDENTIFICACION</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">EMAIL</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoRegistroclientes->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_reg; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE IDENTIFICACION-->
          <td class="text-center">
            <?php echo $filaTemporal->identificacion_reg; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE APELLIDO-->
          <td class="text-center">
            <?php echo $filaTemporal->apellido_reg; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE NOMBRE-->
          <td class="text-center">
            <?php echo $filaTemporal->nombre_reg; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE TELEFONO-->
          <td class="text-center">
            <?php echo $filaTemporal->telefono_reg; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE DIRECCION-->
            <td class="text-center">
              <?php echo $filaTemporal->direccion_reg; ?>
            </td>
            <!--LLAMADO A LOS DATOS DE EMAIL-->
            <td class="text-center">
              <?php echo $filaTemporal->email_reg; ?>
            </td>
          </td>
          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">

            <a class="btn btn-success"  href="<?php echo site_url(); ?>/registrocliente/editar/<?php echo $filaTemporal->id_reg; ?>">
              <i class="fa fa-pen"></i>
            </a>
            <a  onclick="return confirm('ESTAS SEGURO DE ELIMINAR ');"   href="<?php echo site_url(); ?>/registrocliente/procesarEliminacion/<?php echo $filaTemporal->id_cli; ?>" class ="btn btn-danger">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON CLIENTES REGISTRADOS</h1>
  </div>
<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_reg){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI BB</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/registrocliente/procesarEliminacion/"+ id_reg;

        }, true],
        ['<button>NO BB</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>

<script type="text/javascript">
  $("#tbl-registrocliente").DataTable();
</script>
