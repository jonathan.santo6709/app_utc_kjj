<form action="<?php echo site_url(); ?>/bodega/guardarbodega" method="post" id="frm_nuevo_bodega">
    <div class="col-md-12" >
<div class="row">
    <div class="col-md-4" ></div>
        <div class="col-md-8">
          <br>
          <center>
          <h1> <b>FORMULARIO DE REGISTRO BODEGA</b>  </h1>
          <br>
          </center>
          <br>
            <center>

              <br>
              <br>
            <label for=""><b>IDENTIFICACION</b></label><br>
            <input type="number" name="id_bod" id="id_bod"
            placeholder="INGRESE SU IDENTIFICACION" class="form-control" required><br>
            <label for=""><b>NOMBRE DEL PRODUCTO</b></label><br>
            <input type="text" name="nombre_pro" id="nombre_pro" value="" placeholder="INGRESE EL NOMBRE DEL PRODUCTO " class="form-control"><br>
            <label for=""><b>CANTIDAD DEL PRODUCTO</b></label><br>
            <input type="text" name="cantidad_pro" id="cantidad_pro" value="" placeholder="INGRESELA CANTIDAD " class="form-control"><br>
            <br>
            <label for=""><b>DIRECCION</b></label><br>
            <input type="text" name="direccion_cli" id="direccion_cli" value="" placeholder="INGRESE SU DIRECCION" class="form-control"><br>

                <option>SELECCIONE</option>
                <option>ACTIVO</option>
                <option>INACTIVO</option>
                <option>SUSPENDIDO</option>
            </select><br>

            <button class="btn btn-info " type="submit" name="button">REGISTRAR</button>
            &nbsp; &nbsp;&nbsp
            <a href="<?php echo site_url(); ?>/bodega/index" class="btn btn-warning"><i class="fa fa-times"></i>CANCELAR</a>
          </center>
        </div>
    <div class="col-md-2"></div>
    </div>
</div>
</form>
<script type="text/javascript">
    $("#frm_nuevo_bodega").validate({
      rules:{
          id_bod:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_pro:{
          required:true,
          letras:true
        },
      cantidad_pro{
          required:true,
          letras:true
        },
          direccion_pro:{
          required:true

        }
      },
      messages:{

        id_bod:{
          required:"Por favor ingrese la identificacion",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },

        nombre_pro:{
          required:"Ingrese su nombre"
        },
        cantidad_pro:{
          required:"Por favor ingrese la cantidad",

        },
        direccion_pro:{
          required:"Ingrese su direccion de domicilio"


        }
      }
    });
</script>
