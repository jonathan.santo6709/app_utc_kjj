<div class="col-md-12">
<br>
<center>
  <h2> Registroproducto</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/registroproducto/nuevo">Agregar Nuevo Registroproducto</a>
</center>

<?php if ($listadoRegistroproducto): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table " id="tbl-registroproducto">
    <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">IDENTIFICACION</th>
        <th class="text-center">ESTADO</th>
        <th class="text-center">FECHA</th>
        <th class="text-center">DESCRIPCION</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoRegistroclientes->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_reg; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE IDENTIFICACION-->
          <td class="text-center">
            <?php echo $filaTemporal->identificacion_reg; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE ESTADO-->
          <td class="text-center">
            <?php echo $filaTemporal->estado_reg; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE FECHA-->
          <td class="text-center">
            <?php echo $filaTemporal->fecha_reg; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE DESCRIPCION-->
          <td class="text-center">
            <?php echo $filaTemporal->descripcion_reg; ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url() ?>/registroclientes/editar/<?php echo $filaTemporal->id_reg ?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
            <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_reg; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON NINGUN REGISTROCLIENTE</h1>
  </div>
<?php endif; ?>
</div>

<script type="text/javascript">
    function confirmarEliminacion(id_reg){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el recurso de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/registrocliente/procesarEliminacion/"+id_reg;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
<script type="text/javascript">
// incorporar botones de Exportacion
$("#tbl-registrocliente").DataTable();
</script>
