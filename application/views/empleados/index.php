<br>
<center>
  <h2>LISTADO DE EMPLEADOS</h2>
</center>
<hr>
<br>
<center>

  <a href="<?php echo site_url(); ?>/empleados/nuevo">
    <i class="fa fa-plus-circle"></i>
    Agregar Nuevo Empleado
  </a>
</center>


<?php if ($listadoEmpleados): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">IDENTIFICACION</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoEmpleados->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_emp; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE IDENTIFICACION-->
          <td class="text-center">
            <?php echo $filaTemporal->identificacion_emp; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE NOMBRE-->
          <td class="text-center">
            <?php echo $filaTemporal->nombre_emp; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE APELLIDO-->
          <td class="text-center">
            <?php echo $filaTemporal->apellido_emp; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE EMAIL-->
            <td class="text-center">
              <?php echo $filaTemporal->email_emp; ?>
            </td>
          <!--LLAMADO A LOS DATOS DE ESTADO-->
          <td class="text-center">
            <?php if ($filaTemporal->estado_emp=="ACTIVO"): ?>
              <div class="alert alert-success">
                <?php echo $filaTemporal->estado_emp; ?>
              </div>
            <?php else: ?>
              <div class="alert alert-danger">
                <?php echo $filaTemporal->estado_emp; ?>
              </div>
            <?php endif; ?>
          </td>
          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
            <a class="btn btn-success"  href="<?php echo site_url(); ?>/empleados/editar/<?php echo $filaTemporal->id_emp; ?>">
              <i class="fa fa-pen"></i>
            </a>
            <a href="javascript:void(0)"
            onclick="confirmarEliminacion('<?php echo $filaTemporal->id_emp; ?>');"
            class="btn btn-danger">
            <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON EMPLEADOS REGISTRADOS</h1>
  </div>
<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_emp){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/empleados/procesarEliminacion/"+ id_emp;

        }, true],
        ['<button>NO </button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>

<script type="text/javascript">
  $("#tbl-clientes").DataTable();
</script>
