<br>
<h1>
  <center>Edicion de Registro de empleados</center>
</h1>
<form action="<?php echo site_url(); ?>/empleados/procesarActualizacion" method="post">
  <!--CAMBIAR ESTO PARA QUE APARESCA EL ID Y CAMBIAR ARRIBITA NOMAS -->
    <input type="hidden" name="id_emp" id="id_emp" value="<?php echo $empleado->id_emp; ?>">

    <b>Identificación: </b>
    <br>
    <input type="number" class="form-control" name="identificacion_emp" id="identificacion_emp" value="<?php echo $empleado->identificacion_emp; ?>" placeholder="Por favor ingrese la identificacion" class="form-control input-sm " required>
    <br>
    <b>Nombre: </b>
    <br>
    <input type="text" class="form-control" name="nombre_emp" id= "nombre_emp" value="<?php echo $empleado->nombre_emp; ?>" placeholder="Ingrese su nombre" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Apellido: </b>
    <br>
    <input type="text" class="form-control" name="apellido_emp" id= "apellido_emp" value="<?php echo $empleado->apellido_emp; ?>" placeholder="Ingrese el apellido" class="form-control input-sm " required requerid pattern="[A-Za-z]+">
    <br>
    <b>Email: </b>
    <br>
    <input type="text" class="form-control" name="email_emp" id= "email_emp" value="<?php echo $empleado->email_emp; ?>" placeholder="Ingrese su email" class="form-control input-sm " required>
    <br>
    <label class="control-label" for="">ESTADO</label>

    <select  class="form-control" name="estado_emp" id="estado_emp">
        <option value="">SELECIONE</option>
        <option value="ACTIVO">ACTIVO</option>
        <option value="INACTIVO">INACTIVO</option>
    </select>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button"> <i class="fa-solid fa-floppy-disk"></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/empleados/index"> <strong style="color:white;"><i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>
    <script type="text/javascript">
  //activando el estado seleccionado para el cliente
    $('#estado_emp').val('<?php echo $empleado->estado_emp; ?>');
  </script>
</form>
