<br>
<center>
  <h2>LISTADO descuento </h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/descuento/nuevo">Agregar Nuevo</a>
</center>

<?php if ($listadodescuento): ?>

  <table class="table"id="tbl-descuento">
    <thead>
      <tr>
        <th class="text-center">ID_des</th>
        <th class="text-center">porcentaje_desc</th>
        <th class="text-center">id_producto</th>
        <th class="text-center">id_cliente</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadodescuento->result() as $filaTemporal):  ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_des; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->porcentaje_desc; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->id_producto; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->id_cli; ?>
          </td>

          <td class="text-center">

            <a class="btn btn-success"  href="<?php echo site_url(); ?>/descuento/editar/<?php echo $filaTemporal->id_bod; ?>" > <i class="fa fa-pen"></i></a>


          <a  href='javascript:void(0)'
            onclick="confirmarEliminacion('<?php echo$filaTemporal->id_des; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON  REGISTRADOS</h1>
  </div>
<?php endif; ?>

<script type="text/javascript">
  function confirmarEliminacion(id_des){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI </b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/descuento/procesarEliminacion/"+ id_des;

        }, true],
        ['<button>NO</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>
