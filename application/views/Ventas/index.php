<div class="col-md-12">
<br>
<center>
  <h2> ventas</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/ventas/nuevo">Agregar Nuevo ventas</a>
</center>

<?php if ($listadoVentas): ?>

  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">IDENTIFICACION</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">DIRECCION</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoVentas->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_vent; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE IDENTIFICACION-->
          <td class="text-center">
            <?php echo $filaTemporal->identificacion_vent; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE APELLIDO-->
          <td class="text-center">
            <?php echo $filaTemporal->apellido_vent; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE NOMBRE-->
          <td class="text-center">
            <?php echo $filaTemporal->nombre_vent; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE TELEFONO-->
          <td class="text-center">
            <?php echo $filaTemporal->telefono_vent; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE DIRECCION-->
        <td class="text-center">
          <?php echo $filaTemporal->direccion_vent; ?>
        </td>
        <!--LLAMADO A LOS DATOS DE EMAIL-->
      <td class="text-center">
        <?php echo $filaTemporal->email_vent; ?>
      </td>
      <!--LLAMADO A LOS DATOS DE ESTADO-->
    <td class="text-center">
      <?php echo $filaTemporal->estado_vent; ?>
    </td>
    <td class="text-center">
      <a href="<?php echo site_url() ?>/ventas/editar/<?php echo $filaTemporal->id_vent ?>" class="btn btn-warning"> <i class="fa fa-pen"></i> </a>
      <a class="btn btn-danger" href="javascript:void(0)" onclick="confirmarEliminacion('<?php echo $filaTemporal->id_vent; ?>')" ><strong style="color:white;"><i class="fa fa-trash"></i></strong></a>
    </td>
  </tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<div class="alert alert-danger">
<h1>NO SE ENCONTRARON NINGUN VENTA</h1>
</div>
<?php endif; ?>
</div>

<script type="text/javascript">
function confirmarEliminacion(id_vent){
    iziToast.question({
        timeout: 20000,
        close: false,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: 'CONFIRMACIÓN',
        message: '¿Esta seguro de eliminar el recurso de forma pernante?',
        position: 'center',
        buttons: [
            ['<button><b>SI</b></button>', function (instance, toast) {

                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                window.location.href=
                "<?php echo site_url(); ?>/registrocliente/procesarEliminacion/"+id_vent;

            }, true],
            ['<button>NO</button>', function (instance, toast) {

                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

            }],
        ]
    });
}
</script>
<script type="text/javascript">
// incorporar botones de Exportacion
$("#tbl-venta").DataTable();
</script>
