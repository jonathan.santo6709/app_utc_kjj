<br>
<center>
  <h2>LISTADO BODEGA </h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/bodega/nuevo">Agregar Nuevo</a>
</center>

<?php if ($listadobodega): ?>

  <table class="table"id="tbl-bodega">
    <thead>
      <tr>
        <th class="text-center">ID_BOD</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">CANTIDAD</th>
        <th class="text-center">DIRECCION</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadobodega->result() as $filaTemporal):  ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_bod; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_pro; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->cantidad_pro; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->direccion_cli; ?>
          </td>

          <td class="text-center">

            <a class="btn btn-success"  href="<?php echo site_url(); ?>/bodega/editar/<?php echo $filaTemporal->id_bod; ?>" > <i class="fa fa-pen"></i></a>


          <a  href='javascript:void(0)'
            onclick="confirmarEliminacion('<?php echo$filaTemporal->id_bod; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON  REGISTRADOS</h1>
  </div>
<?php endif; ?>

<script type="text/javascript">
  function confirmarEliminacion(id_bod){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI </b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/bodega/procesarEliminacion/"+ id_bod;

        }, true],
        ['<button>NO</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>

<script type="text/javascript">
$("#tbl-bodega").DataTable();
</script>
