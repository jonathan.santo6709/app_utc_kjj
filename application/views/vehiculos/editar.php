<br>
<h1>
  <center>Registro de vehiculos</center>
</h1>
<form action="<?php echo site_url(); ?>/vehiculos/procesarActualizacion" method="post">
  <!--CAMBIAR ESTO PARA QUE APARESCA EL ID Y CAMBIAR ARRIBITA NOMAS -->
    <input type="hidden" name="id_vh" id="id_vh" value="<?php echo $vehiculo->id_vh; ?>">
    <b>Modelo: </b>
    <input type="text" class="form-control" name="modelo_vh" id="modelo_vh" value="<?php echo $vehiculo->modelo_vh; ?>" placeholder="Por favor ingrese el modelo" class="form-control input-sm " required required requerid pattern="[A-Za-z-ñ]+">
    <br>
    <b>Marca: </b>
    <br>
    <input type="text" class="form-control" name="marca_vh" id= "marca_vh"value="<?php echo $vehiculo->marca_vh; ?>" placeholder="Ingrese la marca" class="form-control input-sm " required requerid pattern="[A-Za-z-ñ]+">
    <br>
    <b>Encargado: </b>
    <br>
    <input type="text" class="form-control" name="encargado_vh" id= "encargado_vh"value="<?php echo $vehiculo->encargado_vh; ?>" placeholder="Ingrese el encargado" class="form-control input-sm " required requerid pattern="[A-Za-z-ñ]+">
    <br>
    <label class="control-label" for="">ESTADO</label>

    <select  class="form-control" name="estado_vh" id="estado_vh">
        <option value="">SELECIONE</option>
        <option value="DISPONIBLE">DISPONIBLE</option>
        <option value="DAÑADO">DAÑADO</option>
    </select>
    <br>
    <div align="center" >
    <button class="btn btn-success" type="submit" name="button"> <i class="fa-solid fa-floppy-disk"></i>
      ACTUALIZAR
    </button>
    &nbsp;&nbsp;&nbsp
    <button class="btn btn-danger" type="submit" name="button">
    <a href="<?php echo site_url();?>/vehiculos/index"> <strong style="color:white;"><i class="fa fa-times"></i> CANCELAR</strong></a></button>
    </div>
    <script type="text/javascript">
  //activando el estado seleccionado para el cliente

    $('#estado_vh').val('<?php echo $vehiculo->estado_vh; ?>');
  </script>
</form>
