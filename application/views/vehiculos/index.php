<br>
<center>
  <h2>LISTADO DE VEHICULOS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/vehiculos/nuevo">
    <i class="fa fa-plus-circle"></i>
    Agregar Nuevo vehiculo
  </a>
</center>

<?php if ($listadoVehiculos): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">MODELO</th>
        <th class="text-center">MARCA</th>
        <th class="text-center">ENCARGADO</th>
        <th class="text-center">ESTADO</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoVehiculos->result() as $filaTemporal): ?>
        <tr>
          <!--LLAMADO A LOS DATOS DE ID-->
          <td class="text-center">
            <?php echo $filaTemporal->id_vh; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE MODELO-->
          <td class="text-center">
            <?php echo $filaTemporal->modelo_vh; ?>
          </td>
            <!--LLAMADO A LOS DATOS DE  MARCA-->
          <td class="text-center">
            <?php echo $filaTemporal->marca_vh; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE ENCARGADO-->
          <td class="text-center">
            <?php echo $filaTemporal->encargado_vh; ?>
          </td>
          <!--LLAMADO A LOS DATOS DE ESTADO-->
          <td class="text-center">
            <?php if ($filaTemporal->estado_vh=="DISPONIBLE"): ?>
              <div class="alert alert-success">
                <?php echo $filaTemporal->estado_vh; ?>
              </div>
            <?php else: ?>
              <div class="alert alert-danger">
                <?php echo $filaTemporal->estado_vh; ?>
              </div>
            <?php endif; ?>
          </td>
          <!--OPCIONES EDITAR Y ELIMINAR-->
          <td class="text-center">
            <a class="btn btn-success"  href="<?php echo site_url(); ?>/vehiculos/editar/<?php echo $filaTemporal->id_vh; ?>">
              <i class="fa fa-pen"></i>
            </a>
            <a href="javascript:void(0)"
            onclick="confirmarEliminacion('<?php echo $filaTemporal->id_vh; ?>');"
            class="btn btn-danger">
            <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON VEHICULOS REGISTRADOS</h1>
  </div>
<?php endif; ?>


<script type="text/javascript">
  function confirmarEliminacion(id_vh){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/vehiculos/procesarEliminacion/"+ id_vh;

        }, true],
        ['<button>NO </button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>

<script type="text/javascript">
  $("#tbl-clientes").DataTable();
</script>
