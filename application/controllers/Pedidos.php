<?php
class Pedidos extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PEDIDOS
      $this->load->model("pedido");
      }

      public function index(){
        $data["listadoPedidos"]=$this->pedido->consultarTodos();
        $this->load->view("header");
        $this->load->view("pedidos/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("pedidos/nuevo");
        $this->load->view("footer");
      }
      public function guardarPedido(){
        $datosNuevoPedido=array(
          "fecha_ped"=>$this->input->post("fecha_ped"),
          "monto_ped"=>$this->input->post("monto_ped"),
          "descripcion_ped"=>$this->input->post("descripcion_ped"),
          "estado_ped"=>$this->input->post("estado_ped")

        );
        if($this->pedido->insertar($datosNuevoPedido)){
          //echo "INSERCION EXITOSA";
          $this->session->set_flashdata('confirmacion','Pedido insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("pedidos/index");
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_ped){
      if($this->pedido->eliminar($id_ped)){
        redirect("pedidos/index");
      }else{
        echo "Error al eliminar";
      }
    }
    //ACTUALIZAR
    function editar($id_ped){
              $data["pedido"] = $this->pedido->consultarId($id_ped);
                    $this->load->view('header');
                    $this->load->view('pedidos/editar',$data);
                    $this->load->view('footer');
            }

    public function procesarActualizacion(){
              $id_ped=$this->input->post('id_ped');
              $datosPedidoActualizado=array(
                "fecha_ped"=>$this->input->post("fecha_ped"),
                "monto_ped"=>$this->input->post("monto_ped"),
                "descripcion_ped"=>$this->input->post("descripcion_ped"),
                "estado_ped"=>$this->input->post("estado_ped")
              );

              if ($this->pedido->actualizar($id_ped, $datosPedidoActualizado)) {
                //echo "guardado exitoso";
                $this->session->set_flashdata("confirmacion","Cliente Actualizado exitosamente.");

              }else{
                //echo "error";
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("pedidos/index");
            }
    }//cierre de la clase
?>
