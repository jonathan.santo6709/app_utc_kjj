<?php
class Registroproductos extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PRODUCTOS
      $this->load->model("registroproductos");
      }

      public function index(){
        $data["listadoregistroproductos"]=$this->registroproductos->consultarTodos();
        $this->load->view("header");
        $this->load->view("registroproductos/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("registroproductos/nuevo");
        $this->load->view("footer");
      }
      public function guardarRegistroproductos(){
        $datosNuevoRecursos=array(
          "identificacio_reg"=>$this->input->post("identificacion_reg"),
          "estado_reg"=>$this->input->post("estado_reg"),
          "fecha_reg"=>$this->input->post("fecha_reg"),
          "descripcion_reg"=>$this->input->post("descripcion_reg")

        );
        if($this->Registroproductos->insertar($datosNuevoRegistroproductos)){
          //echo "INSERCION EXITOSA";
          redirect("registroproductos/index");
        }else{
          echo "ERROR AL INSERTAR";
        }
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_reg){
      if($this->registroproductos->eliminar($id_reg)){
        redirect("registroproductos/index");
      }else{
        echo "Error al eliminar";
      }
    }
  }//cierre de la clase
?>
