<?php
class Servicios extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PRODUCTOS
      $this->load->model("servicios");
      }

      public function index(){
        $data["listadoservicios"]=$this->servicios->consultarTodos();
        $this->load->view("header");
        $this->load->view("servicios/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("servicios/nuevo");
        $this->load->view("footer");
      }
      public function guardarServicios(){
        $datosNuevoServicios=array(
          "identificacion_serv"=>$this->input->post("identificacion_serv"),
          "nombre_serv"=>$this->input->post("nombre_serv"),
          "telefono_serv"=>$this->input->post("telefono_serv"),
          "direccion_serv"=>$this->input->post("direccion_serv"),
          "email_serv"=>$this->input->post("email_serv"),
          "estado_serv"=>$this->input->post("estado_serv")
        );
        if($this->Servicios->insertar($datosNuevoServicios)){
          //echo "INSERCION EXITOSA";
          redirect("servicios/index");
        }else{
          echo "ERROR AL INSERTAR";
        }
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_serv){
      if($this->servicios->eliminar($id_serv)){
        redirect("servicios/index");
      }else{
        echo "Error al eliminar";
      }
    }
  }//cierre de la clase
?>
