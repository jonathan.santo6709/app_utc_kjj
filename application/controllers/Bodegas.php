<?php
    class bodega extends CI_Controller{
      public function __construct(){
        parent::__construct();

        $this->load->model("bodega");
        $this->load->model("pais");
      }
      public function index(){
        $data["listadobodega"]=$this->bodega->consultarTodos();
        $this->load->view("header");
        $this->load->view("bodega/index",$data);
        $this->load->view("footer");
      }

      public function editar($id_bod){
        $data['bodega']=$this->cliente->consultarPorId($id_bod);
        $this->load->view("header");
        $this->load->view("bodega/editar",$data);
        $this->load->view("footer");
      }

public function procesarActualizacion(){
$id_bod = $this->input->post("id_bod");
$datosbodegaEditado=array(
  "id_bod"=>$this->input->post("id_bod"),
    "nombre_pro"=>$this->input->post("nombre_pro"),
  "cantidad_pro"=>$this->input->post("telefono_cli"),
  "direccion_cli"=>$this->input->post("direccion_cli"),

);
if ($this->bodega->actualizar($id_bod,$datosbodegaEditado)) {


              $this->session->set_flashdata('confirmacion','EDITADO EXITOSAMENTE');
            } else {
              $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
            }
            redirect("bodega/index");
            }


      public function guardarBodega(){
        $datosNuevobodega=array(
          "id_bod"=>$this->input->post("id_bod"),
            "nombre_pro"=>$this->input->post("nombre_pro"),
          "cantidad_pro"=>$this->input->post("telefono_cli"),
          "direccion_cli"=>$this->input->post("direccion_cli"),

        );
        if($this->bodega->insertar($datosNuevobodega)){

          $this->session->set_flashdata('confirmacion',' insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("bodega/index");
      }

      public function procesarEliminacion($id_bod){
        if ($this->bodega->eliminar($id_bod)){
          redirect ("bodega/index");
        }else {
          echo "ERROR AL ELIMINAR";
        }

      }
    }//cierre de la clase


 ?>
