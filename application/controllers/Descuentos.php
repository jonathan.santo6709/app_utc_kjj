<?php
    class descuento extends CI_Controller{
      public function __construct(){
        parent::__construct();

        $this->load->model("descuento");
        $this->load->model("pais");
      }
      public function index(){
        $data["listadescuento"]=$this->descuento->consultarTodos();
        $this->load->view("header");
        $this->load->view("desceunto/index",$data);
        $this->load->view("footer");
      }


public function procesarActualizacion(){
$id_des= $this->input->post("id_des");
$datosdescuentoEditado=array(
  "id_des"=>$this->input->post("id_des"),
    "porcentaje_desc"=>$this->input->post("porcentaje_desc"),
  "id_producto"=>$this->input->post("id_producto"),
  "id_cli"=>$this->input->post("id_cli"),

);
if ($this->descuento->actualizar($id_desc,$datosdescuentoEditado)) {


              $this->session->set_flashdata('confirmacion','EDITADO EXITOSAMENTE');
            } else {
              $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
            }
            redirect("descuento/index");
            }


      public function guardardescuento(){
        $datosNuevodescuento=array(
          "id_des"=>$this->input->post("id_des"),
            "porcentaje_desc"=>$this->input->post("porcentaje_desc"),
          "id_producto"=>$this->input->post("id_producto"),
          "id_cli"=>$this->input->post("id_cli"),

        );
        if($this->descuento->insertar($datosNuevodescuento)){

          $this->session->set_flashdata('confirmacion',' insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("descuento/index");
      }

      public function procesarEliminacion($id_des){
        if ($this->descuento->eliminar($id_des)){
          redirect ("descuento/index");
        }else {
          echo "ERROR AL ELIMINAR";
        }

      }
    }//cierre de la clase


 ?>
