<?php
class Clientes extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PEDIDOS
      $this->load->model("cliente");
      }

      public function index(){
        $data["listadoClientes"]=$this->cliente->consultarTodos();
        $this->load->view("header");
        $this->load->view("clientes/index",$data);
        $this->load->view("footer");
      }
      public function nuevo(){
            $this->load->view('header');
            $this->load->view('clientes/nuevo');
            $this->load->view('footer');
        }
      public function guardarCliente(){
        $datosNuevoCliente=array(
          "identificacion_cli"=>$this->input->post("identificacion_cli"),
          "nombre_cli"=>$this->input->post("nombre_cli"),
          "apellido_cli"=>$this->input->post("apellido_cli"),
          "direccion_cli"=>$this->input->post("direccion_cli"),
          "telefono_cli"=>$this->input->post("telefono_cli"),
          "estado_cli"=>$this->input->post("estado_cli")

        );
        if($this->cliente->insertar($datosNuevoCliente)){
          //echo "INSERCION EXITOSA";
          //redirect("clientes/index");
          $this->session->set_flashdata('confirmacion','Cliente insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
          //echo "ERROR AL INSERTAR";
        }
        redirect("clientes/index");
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_cli){
      if($this->cliente->eliminar($id_cli)){
        redirect("clientes/index");
      }else{
        echo "Error al eliminar";
      }
    }
    //ACTUALIZAR
    function editar($id_cli){
              $data["cliente"] = $this->cliente->consultarId($id_cli);
                    $this->load->view('header');
                    $this->load->view('clientes/editar',$data);
                    $this->load->view('footer');
            }


    public function procesarActualizacion(){
              $id_cli=$this->input->post('id_cli');
              $datosClienteActualizado=array(
                "identificacion_cli"=>$this->input->post("identificacion_cli"),
                "nombre_cli"=>$this->input->post("nombre_cli"),
                "apellido_cli"=>$this->input->post("apellido_cli"),
                "direccion_cli"=>$this->input->post("direccion_cli"),
                "telefono_cli"=>$this->input->post("telefono_cli"),
                "estado_cli"=>$this->input->post("estado_cli")
              );

              if ($this->cliente->actualizar($id_cli, $datosClienteActualizado)) {
                //echo "guardado exitoso";
                $this->session->set_flashdata("confirmacion","Cliente Actualizado exitosamente.");

              }else{
                //echo "error";
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("clientes/index");
            }

    }//cierre de la clase
?>
