<?php
class Registroclientes extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PRODUCTOS
      $this->load->model("registroclientes");
      }

      public function index(){
        $data["listadoregistroclientes"]=$this->registroclientes->consultarTodos();
        $this->load->view("header");
        $this->load->view("registroclientes/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("registroclientes/nuevo");
        $this->load->view("footer");
      }
      public function guardarRegistroclientes(){
        $datosNuevoRegistroclientes=array(
          "identificacio_reg"=>$this->input->post("contacto_reg"),
          "apellidoreg"=>$this->input->post("catalogos_reg"),
          "nombre_reg"=>$this->input->post("catalogos_reg"),
          "telefono_reg"=>$this->input->post("telefono_reg"),
          "direccion_reg"=>$this->input->post("direccion_reg"),
          "email_reg"=>$this->input->post("email_reg"),
        );
        if($this->registroclientes->insertar($datosNuevoRegistroclientes)){
          //echo "INSERCION EXITOSA";
          redirect("registroclientes/index");
        }else{
          echo "ERROR AL INSERTAR";
        }
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_reg){
      if($this->registroclientes->eliminar($id_reg)){
        redirect("registroclientes/index");
      }else{
        echo "Error al eliminar";
      }
    }
  }//cierre de la clase
?>
