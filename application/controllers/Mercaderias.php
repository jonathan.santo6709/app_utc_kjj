<?php
    class mercaderia extends CI_Controller{
      public function __construct(){
        parent::__construct();

        $this->load->model("mercaderia");
        $this->load->model("pais");
      }
      public function index(){

        $this->load->view("header");
        $this->load->view("mercaderia/index",$data);
        $this->load->view("footer");
      }
            public function editar($id_des){
        $data['mercaderia']=$this->mercaderia->consultarPorId($id_mer);
        $this->load->view("header");
        $this->load->view("mercaderia/editar",$data);
        $this->load->view("footer");
      }

public function procesarActualizacion(){
$id_mer= $this->input->post("id_mer");
$datosmercaderiaEditado=array(
  "id_mer"=>$this->input->post("id_mer"),
    "nombre_mer"=>$this->input->post("nombre_mer"),
  "estado_mer"=>$this->input->post("estado_mer"),

);
if ($this->mercaderia->actualizar($id_mer,$datosmercaderiaEditado)) {

              $this->session->set_flashdata('confirmacion','EDITADO EXITOSAMENTE');
            } else {
              $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
            }
            redirect("mercaderia/index");
            }


      public function guardarBodega(){
        $datosNuevomercaderia=array(
          "id_mer"=>$this->input->post("id_mer"),
            "nombre_mer"=>$this->input->post("nombre_mer"),
          "estado_mer"=>$this->input->post("estado_mer"),

        );
        if($this->mercaderia->insertar($datosNuevomercaderia)){
          
          $this->session->set_flashdata('confirmacion',' insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("mercaderia/index");
      }

      public function procesarEliminacion($id_mer){
        if ($this->mercaderia->eliminar($id_mer)){
          redirect ("mercaderia/index");
        }else {
          echo "ERROR AL ELIMINAR";
        }

      }
    }//cierre de la clase


 ?>
