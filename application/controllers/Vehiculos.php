<?php
class Vehiculos extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO VEHICULOS
      $this->load->model("vehiculo");
      }

      public function index(){
        $data["listadoVehiculos"]=$this->vehiculo->consultarTodos();
        $this->load->view("header");
        $this->load->view("vehiculos/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("vehiculos/nuevo");
        $this->load->view("footer");
      }
      public function guardarVehiculo(){
        $datosNuevoVehiculo=array(
          "modelo_vh"=>$this->input->post("modelo_vh"),
          "marca_vh"=>$this->input->post("marca_vh"),
          "encargado_vh"=>$this->input->post("encargado_vh"),
          "estado_vh"=>$this->input->post("estado_vh")

        );
        if($this->vehiculo->insertar($datosNuevoVehiculo)){
          //echo "INSERCION EXITOSA";
          $this->session->set_flashdata('confirmacion','Vehiculo insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("vehiculos/index");
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_vh){
      if($this->vehiculo->eliminar($id_vh)){
        redirect("vehiculos/index");
      }else{
        echo "Error al eliminar";
      }
    }

    //ACTUALIZAR
    function editar($id_vh){
              $data["vehiculo"] = $this->vehiculo->consultarId($id_vh);
                    $this->load->view('header');
                    $this->load->view('vehiculos/editar',$data);
                    $this->load->view('footer');
            }

    public function procesarActualizacion(){
              $id_vh=$this->input->post('id_vh');
              $datosVehiculoActualizado=array(
                "modelo_vh"=>$this->input->post("modelo_vh"),
                "marca_vh"=>$this->input->post("marca_vh"),
                "encargado_vh"=>$this->input->post("encargado_vh"),
                "estado_vh"=>$this->input->post("estado_vh")
              );

              if ($this->vehiculo->actualizar($id_vh, $datosVehiculoActualizado)) {
                //echo "guardado exitoso";
                $this->session->set_flashdata("confirmacion","Cliente Actualizado exitosamente.");

              }else{
                //echo "error";
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("vehiculos/index");
            }

    }//cierre de la clase
?>
