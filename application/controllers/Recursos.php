<?php
class Recursos extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PRODUCTOS
      $this->load->model("recursos");
      }

      public function index(){
        $data["listadoRecursos"]=$this->recursos->consultarTodos();
        $this->load->view("header");
        $this->load->view("recursos/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("recursos/nuevo");
        $this->load->view("footer");
      }
      public function guardarRecursos(){
        $datosNuevoRecursos=array(
          "contacto_rec"=>$this->input->post("contacto_rec"),
          "catalogos_rec"=>$this->input->post("catalogos_rec"),
          "facturacion_rec"=>$this->input->post("catalogos_rec"),

        );
        if($this->recursos->insertar($datosNuevoRecursos)){
          //echo "INSERCION EXITOSA";
          redirect("recursos/index");
        }else{
          echo "ERROR AL INSERTAR";
        }
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_rec){
      if($this->recursos->eliminar($id_rec)){
        redirect("recursos/index");
      }else{
        echo "Error al eliminar";
    }
   }
  }//cierre de la clase
?>
