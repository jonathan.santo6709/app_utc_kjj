<?php
class Ventas extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PRODUCTOS
      $this->load->model("ventas");
      }

      public function index(){
        $data["listaventas"]=$this->ventas->consultarTodos();
        $this->load->view("header");
        $this->load->view("ventas/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("ventas/nuevo");
        $this->load->view("footer");
      }
      public function guardarVentas(){
        $datosNuevoVentas=array(
          "identificacio_vent"=>$this->input->post("identificacion_vent"),
          "apellido_vent"=>$this->input->post("apellido_vent"),
          "nombre_vent"=>$this->input->post("nombre_vent"),
          "telefono_vent"=>$this->input->post("telefono_vent"),
          "direccion_vent"=>$this->input->post("direccion_vent"),
          "email_vent"=>$this->input->post("email_vent"),
          "estado_vent"=>$this->input->post("estado_vent")
        );
        if($this->Ventas->insertar($datosNuevoVentas)){
          //echo "INSERCION EXITOSA";
          redirect("ventas/index");
        }else{
          echo "ERROR AL INSERTAR";
        }
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_vent){
      if($this->ventas->eliminar($id_vent)){
        redirect("ventas/index");
      }else{
        echo "Error al eliminar";
      }
    }
  }//cierre de la clase
?>
