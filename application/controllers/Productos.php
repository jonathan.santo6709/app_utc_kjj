<?php
class Productos extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO PEDIDOS
      $this->load->model("producto");
      }

      public function index(){
        $data["listadoProductos"]=$this->producto->consultarTodos();
        $this->load->view("header");
        $this->load->view("productos/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("productos/nuevo");
        $this->load->view("footer");
      }
      public function guardarProducto(){
        $datosNuevoProducto=array(
          "nombre_pro"=>$this->input->post("nombre_pro"),
          "categoria_pro"=>$this->input->post("categoria_pro"),
          "marca_pro"=>$this->input->post("marca_pro"),
          "stock_pro"=>$this->input->post("stock_pro"),
          "estado_pro"=>$this->input->post("estado_pro")

        );
        if($this->producto->insertar($datosNuevoProducto)){
          //echo "INSERCION EXITOSA";
          $this->session->set_flashdata('confirmacion','Producto insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("productos/index");
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_pro){
      if($this->producto->eliminar($id_pro)){
        redirect("productos/index");
      }else{
        echo "Error al eliminar";
      }
    }
    //ACTUALIZAR
    function editar($id_pro){
              $data["producto"] = $this->producto->consultarId($id_pro);
                    $this->load->view('header');
                    $this->load->view('productos/editar',$data);
                    $this->load->view('footer');
            }

    public function procesarActualizacion(){
              $id_pro=$this->input->post('id_pro');
              $datosProductoActualizado=array(
                "nombre_pro"=>$this->input->post("nombre_pro"),
                "categoria_pro"=>$this->input->post("categoria_pro"),
                "marca_pro"=>$this->input->post("marca_pro"),
                "stock_pro"=>$this->input->post("stock_pro"),
                "estado_pro"=>$this->input->post("estado_pro")
              );

              if ($this->producto->actualizar($id_pro, $datosProductoActualizado)) {
                //echo "guardado exitoso";
                $this->session->set_flashdata("confirmacion","Cliente Actualizado exitosamente.");

              }else{
                //echo "error";
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("productos/index");
            }
    }//cierre de la clase
?>
