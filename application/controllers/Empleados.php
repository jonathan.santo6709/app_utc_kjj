<?php
class Empleados extends CI_Controller{
    public function __construct(){
      parent::__construct();
      //MODELO CLIENTES
      $this->load->model("empleado");
      }

      public function index(){
        $data["listadoEmpleados"]=$this->empleado->consultarTodos();
        $this->load->view("header");
        $this->load->view("empleados/index",$data);
        $this->load->view("footer");
      }

      public function nuevo(){
        $this->load->view("header");
        $this->load->view("empleados/nuevo");
        $this->load->view("footer");
      }
      public function guardarEmpleado(){
        $datosNuevoEmpleado=array(
          "identificacion_emp"=>$this->input->post("identificacion_emp"),
          "nombre_emp"=>$this->input->post("nombre_emp"),
          "apellido_emp"=>$this->input->post("apellido_emp"),
          "email_emp"=>$this->input->post("email_emp"),
          "estado_emp"=>$this->input->post("estado_emp")
        );
        if($this->empleado->insertar($datosNuevoEmpleado)){
          //echo "INSERCION EXITOSA";
          $this->session->set_flashdata('confirmacion','Empleado insertado exitosamente');

        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("empleados/index");
      }
      //PROCESAR EL BOTON DE ELIMINACION
      function procesarEliminacion($id_emp){
      if($this->empleado->eliminar($id_emp)){
        redirect("empleados/index");
      }else{
        echo "Error al eliminar";
      }
    }

    //ACTUALIZAR
    function editar($id_emp){
              $data["empleado"] = $this->empleado->consultarId($id_emp);
                    $this->load->view('header');
                    $this->load->view('empleados/editar',$data);
                    $this->load->view('footer');
            }


    public function procesarActualizacion(){
              $id_emp=$this->input->post('id_emp');
              $datosEmpleadoActualizado=array(
                "identificacion_emp"=>$this->input->post("identificacion_emp"),
                "nombre_emp"=>$this->input->post("nombre_emp"),
                "apellido_emp"=>$this->input->post("apellido_emp"),
                "email_emp"=>$this->input->post("email_emp"),
                "estado_emp"=>$this->input->post("estado_emp")
              );

              if ($this->empleado->actualizar($id_emp, $datosEmpleadoActualizado)) {
                //echo "guardado exitoso";
                $this->session->set_flashdata("confirmacion","Cliente Actualizado exitosamente.");

              }else{
                //echo "error";
                $this->session->set_flashdata("Error","Error al procesar, intente nuevamente");
              }
              redirect("empleados/index");
            }
    }//cierre de la clase
?>
