<?php
    class promociones extends CI_Controller{
      public function __construct(){
        parent::__construct();

        $this->load->model("promociones");

      }
      public function index(){
        $data["listapromociones"]=$this->promociones->consultarTodos();
        $this->load->view("header");
        $this->load->view("promociones/index",$data);
        $this->load->view("footer");
      }
      public function editar($id_cli){

        $data['promociones']=$this->cliente->consultarPorId($id_pro);
        $this->load->view("header");
        $this->load->view("promociones/editar",$data);
        $this->load->view("footer");
      }

public function procesarActualizacion(){
$id_pro= $this->input->post("id_pro");
$datospromocionesEditado=array(
  "id_bodga"=>$this->input->post("id_bodga"),
    "nombre_pro"=>$this->input->post("nombre_pro"),
  "tipo_pro"=>$this->input->post("tipo_pro"),
  "fk_id_pais"=>$this->input->post("fk_id_pais")
);
if ($this->promociones->actualizar($id_pro,$datospromocionesEditado)) {


              $this->session->set_flashdata('confirmacion','EDITADO EXITOSAMENTE');
            } else {
              $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
            }
            redirect("promociones/index");
            }



      public function guardarpromociones(){
        $datosNuevopromociones=array(
          "id_bodga"=>$this->input->post("id_bodga"),
            "nombre_pro"=>$this->input->post("nombre_pro"),
          "tipo_pro"=>$this->input->post("tipo_pro"),
          "fk_id_pais"=>$this->input->post("fk_id_pais")
        );
        if($this->promociones->insertar($datosNuevopromociones)){
          
          $this->session->set_flashdata('confirmacion',' insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("promociones/index");
      }

      public function procesarEliminacion($id_pro){
        if ($this->promociones->eliminar($id_pro)){
          redirect ("promociones/index");
        }else {
          echo "ERROR AL ELIMINAR";
        }

      }
    }//cierre de la clase


 ?>
