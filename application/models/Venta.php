<?php
 class Venta extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("venta",$datos);
}
//funcion para actualizar
public function actualizar($id_vent,$datos){
  $this->db->where('id_vent',$id_vent);
  return $this->db->update('venta',$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoVenta=$this->db->get("venta");
  if($listadoVenta->num_rows()>0){
    return $listadoVneta;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_vent){
      $this->db->where("id_vent",$id_vent);
      return $this->db->delete("venta");
  }
}//cierre de la clase
?>
