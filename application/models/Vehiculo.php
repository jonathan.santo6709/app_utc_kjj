<?php
 class Vehiculo extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("vehiculo",$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoVehiculos=$this->db->get("vehiculo");
  if($listadoVehiculos->num_rows()>0){
    return $listadoVehiculos;//Retorno cuando si hay vehiculos
  }else{
    return false;//Retorno cuando no hay vehiculos.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_vh){
      $this->db->where("id_vh",$id_vh);
      return $this->db->delete("vehiculo");
  }
  //actualizar
  //MODIFIC
    public function consultarId($id_vh){
        $this->db->where("id_vh",$id_vh);
        $listadoVehiculos=$this->db->get('vehiculo');
        if ($listadoVehiculos->num_rows()>0) {
          //clientes
          return $listadoVehiculos->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_vh,$datos){
         $this->db->where("id_vh",$id_vh);
         return $this->db->update("vehiculo",$datos);
       }
  }//cierre de la clasee

?>
