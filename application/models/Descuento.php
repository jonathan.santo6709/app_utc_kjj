<?php
 class descuento extends CI_Model{
  public function __construct(){
   parent::__construct();
}

public function insertar($datos){
  return $this->db -> insert("descuento",$datos);
}

public function actualizar($id_des,$datos){
  $this->db->where('id_des',$id_des);
  return $this->db->update('descuento',$datos);
}

public function consultarPorId($id_des){
  $this->db->where('id_des',$id_des);
  $descuento=$this->db->get("descuento");
  if ($descuento->num_rows()>0){

    return $descuento->row();
  }else{

    return false;

  }
 }

/
public function consultarTodos(){
    $listadodescuento=$this->db ->get("descuento");
  if ($listadodescuento->num_rows()>0){

    return $listadodescuento;
  }else{

    return false;

  }
 }


 public function eliminar($id_des){
   $this->db->where("id_des",$id_des);
   return $this->db->delete("descuento");

 }


}

 ?>
