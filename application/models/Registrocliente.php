<?php
 class Registrocliente extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("registrocliente",$datos);
}
//funcion para actualizar
public function actualizar($id_reg,$datos){
  $this->db->where('id_reg',$id_reg);
  return $this->db->update('registrocliente',$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoRegistrocliente=$this->db->get("registrocliente");
  if($listadoRegistrocliente->num_rows()>0){
    return $listadoRegistrocliente;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_reg){
      $this->db->where("id_reg",$id_reg);
      return $this->db->delete("registrocliente");
  }
}//cierre de la clase
?>
