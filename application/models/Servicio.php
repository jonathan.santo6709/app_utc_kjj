<?php
 class Servicio extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("servicio",$datos);
}
//funcion para actualizar
public function actualizar($id_serv,$datos){
  $this->db->where('id_serv',$id_serv);
  return $this->db->update('servicio',$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoServicio=$this->db->get("servicio");
  if($listadoServicio->num_rows()>0){
    return $listadoServicio;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_serv){
      $this->db->where("id_serv",$id_serv);
      return $this->db->delete("servicio");
  }
}//cierre de la clase
?>
