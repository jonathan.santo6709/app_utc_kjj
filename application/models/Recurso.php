<?php
 class Recurso extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("recurso",$datos);
}
//funcion para actualizar
public function actualizar($id_rec,$datos){
  $this->db->where('id_rec',$id_rec);
  return $this->db->update('recurso',$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoRecurso=$this->db->get("recurso");
  if($listadoRecurso->num_rows()>0){
    return $listadoRecurso;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_rec){
      $this->db->where("id_rec",$id_rec);
      return $this->db->delete("recursos");
  }
}//cierre de la clase
?>
