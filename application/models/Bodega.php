<?php
 class bodega extends CI_Model{
  public function __construct(){
   parent::__construct();
}

public function insertar($datos){
  return $this->db -> insert("bodega",$datos);
}

public function actualizar($id_bod,$datos){
  $this->db->where('id_bod',$id_bod);
  return $this->db->update('bodega',$datos);
}

public function consultarPorId($id_bod){
  $this->db->where('id_bod',$id_bod);
  $bodega=$this->db->get("bodega");
  if ($bodega->num_rows()>0){

    return $bodega->row();
  }else{

    return false;

  }
 }

public function consultarTodos(){
  $listadobodega=$this->db ->get("bodega");
  if ($listadobodega->num_rows()>0){

    return $listadobodega;
  }else{

    return false;

  }
 }


 public function eliminar($id_bod){
   $this->db->where("id_bod",$id_bod);
   return $this->db->delete("bodega");

 }


}

 ?>
