<?php
 class Empleado extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("empleado",$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoEmpleados=$this->db->get("empleado");
  if($listadoEmpleados->num_rows()>0){
    return $listadoEmpleados;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_emp){
      $this->db->where("id_emp",$id_emp);
      return $this->db->delete("empleado");
  }


  //actualizar
  //MODIFIC
    public function consultarId($id_emp){
        $this->db->where("id_emp",$id_emp);
        $listadoEmpleados=$this->db->get('empleado');
        if ($listadoEmpleados->num_rows()>0) {
          //clientes
          return $listadoEmpleados->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_emp,$datos){
         $this->db->where("id_emp",$id_emp);
         return $this->db->update("empleado",$datos);
       }

}//cierre de la clase

?>
