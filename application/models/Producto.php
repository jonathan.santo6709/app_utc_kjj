<?php
 class Producto extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("producto",$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoProductos=$this->db->get("producto");
  if($listadoProductos->num_rows()>0){
    return $listadoProductos;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_pro){
      $this->db->where("id_pro",$id_pro);
      return $this->db->delete("producto");
  }
  //actualizar
  //MODIFIC
    public function consultarId($id_pro){
        $this->db->where("id_pro",$id_pro);
        $listadoProductos=$this->db->get('producto');
        if ($listadoProductos->num_rows()>0) {
          //clientes
          return $listadoProductos->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_pro,$datos){
         $this->db->where("id_pro",$id_pro);
         return $this->db->update("producto",$datos);
       }
}//cierre de la clase

?>
