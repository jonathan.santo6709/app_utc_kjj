<?php
 class Registroproducto extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("registroproducto",$datos);
}
//funcion para actualizar
public function actualizar($id_reg,$datos){
  $this->db->where('id_reg',$id_reg);
  return $this->db->update('registroproducto',$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoRegistroproducto=$this->db->get("registroproducto");
  if($listadoRegistroproducto->num_rows()>0){
    return $listadoRegistroproducto;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_reg){
      $this->db->where("id_reg",$id_reg);
      return $this->db->delete("registroproducto");
  }
}//cierre de la clase
?>
