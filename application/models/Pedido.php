<?php
 class Pedido extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funcionn para insertar datos
public function insertar($datos){
  return $this->db->insert("pedido",$datos);
}
//funcion par consultar todos los clientes
public function consultarTodos(){
  $listadoPedidos=$this->db->get("pedido");
  if($listadoPedidos->num_rows()>0){
    return $listadoPedidos;//Retorno cuando si hay clientes
  }else{
    return false;//Retorno cuando no hay clientes.
  }
  }
  // funcion para que de accion a eliminar
  public function eliminar($id_ped){
      $this->db->where("id_ped",$id_ped);
      return $this->db->delete("pedido");
  }
  //actualizar
  //MODIFIC
    public function consultarId($id_ped){
        $this->db->where("id_ped",$id_ped);
        $listadoPedidos=$this->db->get('pedido');
        if ($listadoPedidos->num_rows()>0) {
          //clientes
          return $listadoPedidos->row();
        }else {
          // sin datos
          return false;
        }
       }
       public function actualizar($id_ped,$datos){
         $this->db->where("id_ped",$id_ped);
         return $this->db->update("pedido",$datos);
       }

}//cierre de la clase

?>
