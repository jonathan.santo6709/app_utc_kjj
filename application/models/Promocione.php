<?php
 class promociones extends CI_Model{
  public function __construct(){
   parent::__construct();
}

public function insertar($datos){
  return $this->db -> insert("promociones",$datos);
}

public function actualizar($id_pro,$datos){
  $this->db->where('id_pro',$id_pro);
  return $this->db->update('promociones',$datos);
}

public function consultarPorId($id_pro){
  $this->db->where('id_pro',$id_pro);
  $promociones=$this->db->get("promociones");
  if ($promociones->num_rows()>0){

    return $promociones->row();
  }else{

    return false;

  }
 }

/
public function consultarTodos(){
    $listadopromociones=$this->db ->get("promociones");
  if ($listapromociones->num_rows()>0){

    return $listadopromociones;
  }else{

    return false;

  }
 }


 public function eliminar($id_prp){
   $this->db->where("id_pro",$id_pro);
   return $this->db->delete("promociones");

 }


}

 ?>
